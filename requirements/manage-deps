#!/bin/bash


REQUIREMENTS_IN="$(ls *.in)"
REQUIREMENTS_TXT="$(ls *.txt)"
MAIN_REQUIREMENT_FILE="requirements.in"
LOCKED_MAIN_REQUIREMENTS_FILE="requirements.txt"
PYTHON=python


HELP_STRING=$(cat <<EOF
    Usage: $0 {lock|upgrade-all|help}

    pip-tools must be installed!

    lock { A requirement .in file | default: "$MAIN_REQUIREMENT_FILE" } - Create a requirements.*.txt file from the passed in file with locked dependencies.
    upgrade-all - Try to upgrade the dependencies in  each requirement.*.txt file and create each requirements.*.txt
EOF
)


lock_main() {

    # Copy all requirements into main requirements file, removing duplicates
    cat -s $REQUIREMENTS_IN | tr -s '\n' '\n' | sort | uniq > $MAIN_REQUIREMENT_FILE

    # Create new locked requirements.txt file
    lock "$MAIN_REQUIREMENT_FILE"
}


lock() {

    if [[ -z "$1" ]]; then
        echo "Got empty var"
        exit 1
    fi

    "$PYTHON" -m piptools compile --no-emit-trusted-host "$1" 1> /dev/null
}


upgrade() {

    for file in ${REQUIREMENTS_TXT[@]}; do
        if [[ "$file" = "$LOCKED_MAIN_REQUIREMENTS_FILE" ]]; then
            continue
        fi
       "$PYTHON" -m piptools compile --upgrade --no-emit-trusted-host "$file" 1> /dev/null
    done

    lock_main
}


case "$1" in

    lock)
        if [[ -z "$2" ]]; then
            lock_main
        else
            lock "$2"
        fi
        exit $?;
    ;;
    upgrade-all)
        upgrade
        exit $?;
    ;;
	help)
        echo "$HELP_STRING";
        exit $?;
    ;;
    *)
        echo "$HELP_STRING";
        exit $?;
    ;;
esac
