# Copyright (C) 2021 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License' is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from grpc import aio
import pytest

from buildgrid.client.authentication import AsyncAuthMetadataClientInterceptor
from buildgrid.client.channel import AsyncRequestMetadataInterceptor
from buildgrid.settings import (
    REQUEST_METADATA_HEADER_NAME,
    REQUEST_METADATA_TOOL_NAME,
    REQUEST_METADATA_TOOL_VERSION
)


ACTION_ID = 'test-action'
TOOL_INVOCATION_ID = 'inv-01'
CORRELATED_INVOCATIONS_ID = 'corr-01'


@pytest.fixture
def request_metadata():
    request_metadata = remote_execution_pb2.RequestMetadata()
    request_metadata.tool_details.tool_name = REQUEST_METADATA_TOOL_NAME
    request_metadata.tool_details.tool_version = REQUEST_METADATA_TOOL_VERSION
    request_metadata.action_id = ACTION_ID
    request_metadata.tool_invocation_id = TOOL_INVOCATION_ID
    request_metadata.correlated_invocations_id = CORRELATED_INVOCATIONS_ID
    return request_metadata


@pytest.fixture
def original_details():
    return aio.ClientCallDetails(
        method='/TestMethod',
        timeout=60,
        metadata=(),
        credentials=None,
        wait_for_ready=True
    )


@pytest.fixture
def continuation():
    async def _continuation(details, request):
        return details
    return _continuation


@pytest.mark.asyncio
async def test_async_auth_client_interceptor_unary_unary(original_details, continuation):
    interceptor = AsyncAuthMetadataClientInterceptor(auth_token='test-token')
    new_details = await interceptor.intercept_unary_unary(continuation, original_details, None)

    expected_metadata = ('authorization', 'Bearer test-token')
    assert expected_metadata in new_details.metadata
    assert expected_metadata not in original_details.metadata


@pytest.mark.asyncio
async def test_async_auth_client_interceptor_stream_unary(original_details, continuation):
    interceptor = AsyncAuthMetadataClientInterceptor(auth_token='test-token')
    new_details = await interceptor.intercept_stream_unary(continuation, original_details, None)

    expected_metadata = ('authorization', 'Bearer test-token')
    assert expected_metadata in new_details.metadata
    assert expected_metadata not in original_details.metadata


@pytest.mark.asyncio
async def test_async_auth_client_interceptor_unary_stream(original_details, continuation):
    interceptor = AsyncAuthMetadataClientInterceptor(auth_token='test-token')
    new_details = await interceptor.intercept_unary_stream(continuation, original_details, None)

    expected_metadata = ('authorization', 'Bearer test-token')
    assert expected_metadata in new_details.metadata
    assert expected_metadata not in original_details.metadata


@pytest.mark.asyncio
async def test_async_auth_client_interceptor_stream_stream(original_details, continuation):
    interceptor = AsyncAuthMetadataClientInterceptor(auth_token='test-token')
    new_details = await interceptor.intercept_stream_stream(continuation, original_details, None)

    expected_metadata = ('authorization', 'Bearer test-token')
    assert expected_metadata in new_details.metadata
    assert expected_metadata not in original_details.metadata


@pytest.mark.asyncio
async def test_async_request_metadata_interceptor_unary_unary(
        original_details, continuation, request_metadata):
    interceptor = AsyncRequestMetadataInterceptor(
        action_id=ACTION_ID,
        tool_invocation_id=TOOL_INVOCATION_ID,
        correlated_invocations_id=CORRELATED_INVOCATIONS_ID
    )
    new_details = await interceptor.intercept_unary_unary(
        continuation, original_details, None)

    expected_metadata = (REQUEST_METADATA_HEADER_NAME, request_metadata.SerializeToString())
    assert expected_metadata in new_details.metadata
    assert expected_metadata not in original_details.metadata


@pytest.mark.asyncio
async def test_async_request_metadata_interceptor_stream_unary(
        original_details, continuation, request_metadata):
    interceptor = AsyncRequestMetadataInterceptor(
        action_id=ACTION_ID,
        tool_invocation_id=TOOL_INVOCATION_ID,
        correlated_invocations_id=CORRELATED_INVOCATIONS_ID
    )
    new_details = await interceptor.intercept_stream_unary(
        continuation, original_details, None)

    expected_metadata = (REQUEST_METADATA_HEADER_NAME, request_metadata.SerializeToString())
    assert expected_metadata in new_details.metadata
    assert expected_metadata not in original_details.metadata


@pytest.mark.asyncio
async def test_async_request_metadata_interceptor_unary_stream(
        original_details, continuation, request_metadata):
    interceptor = AsyncRequestMetadataInterceptor(
        action_id=ACTION_ID,
        tool_invocation_id=TOOL_INVOCATION_ID,
        correlated_invocations_id=CORRELATED_INVOCATIONS_ID
    )
    new_details = await interceptor.intercept_unary_stream(
        continuation, original_details, None)

    expected_metadata = (REQUEST_METADATA_HEADER_NAME, request_metadata.SerializeToString())
    assert expected_metadata in new_details.metadata
    assert expected_metadata not in original_details.metadata


@pytest.mark.asyncio
async def test_async_request_metadata_interceptor_stream_stream(
        original_details, continuation, request_metadata):
    interceptor = AsyncRequestMetadataInterceptor(
        action_id=ACTION_ID,
        tool_invocation_id=TOOL_INVOCATION_ID,
        correlated_invocations_id=CORRELATED_INVOCATIONS_ID
    )
    new_details = await interceptor.intercept_stream_stream(
        continuation, original_details, None)


    expected_metadata = (REQUEST_METADATA_HEADER_NAME, request_metadata.SerializeToString())
    assert expected_metadata in new_details.metadata
    assert expected_metadata not in original_details.metadata

