# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import re
from unittest import mock
import uuid

import grpc
from grpc._server import _Context
import pytest

from buildgrid._exceptions import NotFoundError, StreamFinishedError
from buildgrid._protos.google.bytestream.bytestream_pb2 import (
    QueryWriteStatusRequest,
    ReadRequest,
    ReadResponse,
    WriteRequest,
    WriteResponse
)
from buildgrid._protos.build.bazel.remote.logstream.v1.remote_logstream_pb2 import (
    CreateLogStreamRequest,
    LogStream
)
from buildgrid.server.cas.service import ByteStreamService
from buildgrid.server.cas.instance import ByteStreamInstance
from buildgrid.server.cas.logstream import service
from buildgrid.server.cas.logstream.instance import LogStreamInstance
from buildgrid.server.cas.logstream.service import LogStreamService
from buildgrid.server.cas.logstream.stream_storage.stream_storage_abc import StreamLength
from buildgrid.server.cas.logstream.stream_storage.memory import MemoryStreamStorage


server = mock.MagicMock()


@pytest.fixture
def context():
    cxt = mock.MagicMock(spec=_Context)
    yield cxt


@pytest.fixture()
def storage(request):
    yield MemoryStreamStorage()


@pytest.fixture(params=['', 'test-'])
def logstream_instance(request, storage):
    yield LogStreamInstance(request.param, storage=storage)


@pytest.fixture()
def bytestream_instance(request, storage):
    yield ByteStreamInstance(stream_storage=storage)


@pytest.fixture()
def logstream_service(logstream_instance):
    logstream = LogStreamService(server)
    logstream.add_instance('', logstream_instance)
    yield logstream


@pytest.fixture()
def bytestream_service(bytestream_instance):
    bytestream = ByteStreamService(server)
    bytestream.add_instance('', bytestream_instance)
    yield bytestream


def test_create_logstream(logstream_service, context):
    parent = 'test'
    request = CreateLogStreamRequest(parent=f'/{parent}')
    response = logstream_service.CreateLogStream(request, context)
    assert isinstance(response, LogStream)

    instance = logstream_service._get_instance('')
    match = re.fullmatch(
        f'^{parent}/logStreams/{instance._prefix}[0-9a-f-]*$', response.name)
    assert match is not None


def test_create_logstream_repeated_calls(logstream_service, context):
    parent = 'test'
    request = CreateLogStreamRequest(parent=f'/{parent}')
    response_1 = logstream_service.CreateLogStream(request, context)
    response_2 = logstream_service.CreateLogStream(request, context)
    assert len(response_1.name) > 0
    assert len(response_2.name) > 0

    assert len(response_1.write_resource_name) > 0
    assert len(response_2.write_resource_name) > 0

    assert response_1.name != response_2.name
    assert response_1.write_resource_name != response_2.write_resource_name


def test_logstream_bytestream_read_no_stream(bytestream_service, context):
    resource = '/test/logStreams/abcdef'
    request = ReadRequest(resource_name=resource)
    responses = bytestream_service.Read(request, context)
    response = next(responses)
    context.set_code.assert_called_with(grpc.StatusCode.NOT_FOUND)
    assert response == ReadResponse()


def test_logstream_bytestream_write_no_stream(bytestream_service, context):
    resource = '/test/logStreams/abcdef/abcdef'

    def requests():
        yield WriteRequest(resource_name=resource, data=b'test')

    response = bytestream_service.Write(requests(), context)
    context.set_code.assert_called_with(grpc.StatusCode.NOT_FOUND)
    assert response == WriteResponse()


def test_logstream_bytestream_read(logstream_service, bytestream_service, context):
    parent = 'test'
    request = CreateLogStreamRequest(parent=f'{parent}')
    response = logstream_service.CreateLogStream(request, context)

    read_name = response.name
    write_name = response.write_resource_name

    instance = logstream_service._get_instance('')
    instance._stream_store.append_to_stream(write_name, b'test log entry', mark_finished=True)

    request = ReadRequest(resource_name=read_name)
    responses = bytestream_service.Read(request, context)
    response = next(responses)
    assert response.data == b'test log entry'


def test_logstream_bytestream_write(logstream_service, bytestream_service, context):
    request = CreateLogStreamRequest(parent='test-parent')
    response = logstream_service.CreateLogStream(request, context)

    read_name = response.name
    write_name = response.write_resource_name

    stream_store = logstream_service._get_instance('')._stream_store

    streamlength = stream_store.stream_length(read_name)
    assert streamlength.finished is False
    assert streamlength.length == 0

    def requests():
        yield WriteRequest(resource_name=write_name, data=b'test log entry')

    response = bytestream_service.Write(requests(), context)

    streamchunk = stream_store.read_stream_chunk_blocking(read_name, read_offset_bytes=0)
    assert streamchunk.finished is True
    assert streamchunk.data == b'test log entry'


def test_logstream_bytestream_write_specific_resource_name(
        logstream_service, bytestream_service, context):
    request = CreateLogStreamRequest(parent='test-parent')
    response = logstream_service.CreateLogStream(request, context)

    read_name = response.name
    write_name = response.write_resource_name

    stream_store = logstream_service._get_instance('')._stream_store

    streamlength = stream_store.stream_length(read_name)
    assert streamlength.finished is False
    assert streamlength.length == 0

    def requests(resource):
        yield WriteRequest(resource_name=resource, data=b'another test log entry')

    response = bytestream_service.Write(requests(read_name), context)
    context.set_code.assert_called_with(grpc.StatusCode.INVALID_ARGUMENT)
    assert response == WriteResponse()

    response = bytestream_service.Write(requests(write_name), context)

    streamchunk = stream_store.read_stream_chunk_blocking(read_name, read_offset_bytes=0)
    assert streamchunk.finished is True
    assert streamchunk.data == b'another test log entry'


def test_logstream_bytestream_no_early_cleanup_write_only(
        logstream_service, bytestream_service, context):
    request = CreateLogStreamRequest(parent='test-parent')
    response = logstream_service.CreateLogStream(request, context)

    read_name = response.name
    write_name = response.write_resource_name

    stream_store = logstream_service._get_instance('')._stream_store

    def requests(resource):
        yield WriteRequest(resource_name=resource,
                           data=b'test log entry',
                           finish_write=True)

    # Temporarily add a fake reader
    stream_store.new_client_streaming(read_name)

    # Check that committing a Write is successfully tracked, and the
    # stream is cleaned up after the last reader leaves
    response = bytestream_service.Write(requests(write_name), context)

    streamlength = stream_store.stream_length(read_name)
    assert streamlength.finished is True
    assert streamlength.length == len(b'test log entry')

    # No more clients trying to stream; should still see this in memory, though
    stream_store.streaming_client_left(read_name)
    assert stream_store.stream_length(read_name) == streamlength


def test_logstream_bytestream_no_early_cleanup_after_read(
        logstream_service, bytestream_service, context):
    request = CreateLogStreamRequest(parent='test-parent')
    response = logstream_service.CreateLogStream(request, context)

    read_name = response.name
    write_name = response.write_resource_name

    stream_store = logstream_service._get_instance('')._stream_store

    # Temporarily add a fake reader
    stream_store.new_client_streaming(read_name)

    def requests(resource):
        yield WriteRequest(resource_name=resource,
                           data=b'test log entry with reader',
                           finish_write=True)

    # Check that committing a Write is successfully tracked, and the
    # stream is cleaned up after the last reader leaves
    response = bytestream_service.Write(requests(write_name), context)

    streamlength = stream_store.stream_length(read_name)
    assert streamlength.finished is True
    assert streamlength.length == len(b'test log entry with reader')

    # Try to change stream from finished -> not finished (should fail)
    with pytest.raises(StreamFinishedError):
        stream_store.append_to_stream(write_name, message=None, mark_finished=False)

    # Force the stream to not be committed, since we're pretending this
    # is running in parallel.
    # (This is MemoryStreamStorage specific)
    stream_store._streams[read_name]._stream_length = StreamLength(streamlength.length, False)

    streamlength = stream_store.stream_length(read_name)
    assert streamlength.finished is False
    assert streamlength.length == len(b'test log entry with reader')

    # Re-commit the stream
    stream_store.append_to_stream(write_name, message=None, mark_finished=False)

    # "Disconnect" the fake reader
    bs_instance = bytestream_service._get_instance('')
    bs_instance.disconnect_logstream_reader(read_name)

    # No more clients trying to stream; this should not be cleaned up
    # until LRU kicks in
    assert stream_store.stream_length(read_name) == streamlength


def test_logstream_bytestream_query_write_status_blocks_with_no_reader(
        logstream_service, bytestream_service, context):
    parent = 'test'
    request = CreateLogStreamRequest(parent=f'/{parent}')
    logstream = logstream_service.CreateLogStream(request, context)

    instance = logstream_service._get_instance('')

    query_write_status = QueryWriteStatusRequest(
        resource_name=logstream.write_resource_name)

    active_statuses = [False, True]

    def is_active():
        return active_statuses.pop()

    context.is_active = is_active

    bs_instance = bytestream_service._get_instance('')
    bs_instance._query_activity_timeout = 2
    response = bytestream_service.QueryWriteStatus(query_write_status, context)
    context.set_code.assert_called_with(grpc.StatusCode.NOT_FOUND)


def test_logstream_bytestream_query_write_status_empty(
        logstream_service, bytestream_service, context):
    parent = 'test'
    request = CreateLogStreamRequest(parent=f'/{parent}')
    logstream = logstream_service.CreateLogStream(request, context)

    instance = logstream_service._get_instance('')

    streamlength = instance._stream_store.stream_length(logstream.name)

    query_write_status = QueryWriteStatusRequest(
        resource_name=logstream.write_resource_name)

    # Pretend that there's a reader.
    instance._stream_store.new_client_streaming(logstream.name)
    response = bytestream_service.QueryWriteStatus(query_write_status, context)
    assert response.committed_size == streamlength.length
