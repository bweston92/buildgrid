# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from buildgrid._app.settings import rmq_parser

import jsonschema

import tempfile
import pytest

@pytest.fixture
def test_file():
    yield tempfile.NamedTemporaryFile(mode='w+t')

def test_simple_valid_config(test_file):
    config = """platform-queues:
  - platform: isa=x86_64;osfamily=linux
    queues:
      - isa=x86_64;osfamily=linux
      - osfamily=linux"""
    test_file.write(config)
    test_file.flush()

    parsed_config = rmq_parser.BotsService.read_platform_queues_from_file(test_file.name)

    platform = 'isa=x86_64;osfamily=linux'
    assert parsed_config.keys() == {platform}

    # And it is bound to the expected queues:
    platform_queues = set(parsed_config[platform])
    assert platform_queues == {'osfamily=linux', 'isa=x86_64;osfamily=linux'}


def test_valid_config_with_multiple_platforms(test_file):
    config = """platform-queues:
  - platform: osfamily=linux
    queues:
      - isa=x86_64;osfamily=linux
      - osfamily=linux
  - platform: osfamily=windows
    queues:
      - osfamily=windows"""
    test_file.write(config)
    test_file.flush()

    parsed_config = rmq_parser.BotsService.read_platform_queues_from_file(test_file.name)
    assert parsed_config.keys() == {'osfamily=linux', 'osfamily=windows'}

    windows_queues = parsed_config['osfamily=windows']
    assert windows_queues == {'osfamily=windows'}

    linux_queues = parsed_config['osfamily=linux']
    assert set(linux_queues) == {'isa=x86_64;osfamily=linux', 'osfamily=linux'}


def test_wrong_separator(test_file):
    config = """platform-queues:
  - platform: isa=x86_64:osfamily=linux
    queues:
      - isa=x86_64:osfamily=linux
      - osfamily=linux"""
    test_file.write(config)
    test_file.flush()

    with pytest.raises(jsonschema.ValidationError):
        _ = rmq_parser.BotsService.read_platform_queues_from_file(test_file.name)

def test_repeated_platform_properties(test_file):
    config = """platform-queues:
  - platform: osfamily=linux;isa=x86_64     # <- Same value
    queues:                                 #
      - osfamily=linux;isa=x86_64           #
  - platform: osfamily=linux;isa=x86_64     # <-
    queues:
      - osfamily=linux"""
    test_file.write(config)
    test_file.flush()

    with pytest.raises(jsonschema.ValidationError):
        _ = rmq_parser.BotsService.read_platform_queues_from_file(test_file.name)

def test_unsorted_platform_properties(test_file):
    config = """platform-queues:
  - platform: osfamily=linux;isa=x86_64
    queues:
      - osfamily=linux"""
    test_file.write(config)
    test_file.flush()

    with pytest.raises(jsonschema.ValidationError):
        _ = rmq_parser.BotsService.read_platform_queues_from_file(test_file.name)

def test_unsorted_queue_name(test_file):
    config = """platform-queues:
  - platform: osfamily=linux
    queues:
      - osfamily=linux;isa=x86_64"""
    test_file.write(config)
    test_file.flush()

    with pytest.raises(jsonschema.ValidationError):
        _ = rmq_parser.BotsService.read_platform_queues_from_file(test_file.name)

def test_repeated_queue_name(test_file):
    config = """platform-queues:
  - platform: osfamily=linux
    queues:
      - osfamily=linux
      - osfamily=linux
    """
    test_file.write(config)
    test_file.flush()

    parsed_config = rmq_parser.BotsService.read_platform_queues_from_file(test_file.name)
    assert parsed_config['osfamily=linux'] == {'osfamily=linux'}
