#!/bin/bash
# This is the BuildGrid start script

source /mnt/scripts/defaults.sh
wait_for_pges_up

config="/mnt/configs/bgd-db-init.yml"
echo "Starting buildgrid-db-init with config: [$config]"

sleep ${STRESS_TESTING_GRACE_PERIOD:-1}

/app/env/bin/bgd server start -vv "$config"
