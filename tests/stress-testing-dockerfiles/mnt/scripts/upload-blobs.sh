#!/bin/bash

filled_up_high=
test_end=$((SECONDS+$TEST_LENGTH))
casupload=/buildbox-e2e-build/buildbox-tools/cpp/build/casupload/casupload
while true;
do
    size=$(find /minio/buildgrid-bucket -type f -print0 | \
    du -scb --files0-from=- | grep total)
    declare -i num=$(echo $size | awk '{print $1}')

    if [[ -z $NOT_RUN_IN_STAGES ]] && [[ $num -gt $HIGH_WATERMARK ]]
    then
        filled_up_high=true
    fi

    if [[ -z $filled_up_high ]]
    then
        file_name=$(mktemp XXXXXXX)
        file_size=$(shuf -i 0-$MAX_FILE_SIZE -n 1)
        head -c $file_size </dev/urandom >> $file_name
        $casupload --cas-server=$RECC_SERVER $file_name > /dev/null 2>&1
    fi
done
