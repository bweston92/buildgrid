#!/bin/bash

wait_for_host_port(){
	host=$1
	port=$2
	echo -n "Waiting for $host:$port to become available: "

	# Use one of the available tools
	if [[ ! -z $(which nc) ]]; then
		echo "Using 'nc'"
		while ! nc -z "$host" "$port"; do sleep 10; done
	elif [[ ! -z $(which python3) ]]; then
		echo "Using python script"
		while ! python3 /mnt/scripts/is-port-open.py "$host" "$port"; do sleep 10; done
	else
		echo "No tool available to check server up, skipping check."
	fi
}

wait_for_bgd_up(){
	wait_for_host_port "buildgrid" "50051"
}

wait_for_pges_up(){
	wait_for_host_port "database" "5432"
}

wait_for_pges_init_up(){
	wait_for_host_port "buildgrid-db-initializer" "55555"
}

wait_for_pges_up_and_ready_if_config() {
	config=$1
	# Check if postgres is used in config:
	if grep "postgresql://" "$config"; then
		wait_for_pges_up
		wait_for_pges_init_up
	else
		echo "Config doesn't use postgres; not waiting."
	fi
}
