# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from unittest import mock
from collections import namedtuple
import pytest
import queue

from grpc._server import _Context

from buildgrid.server.execution.instance import ExecutionInstance
from buildgrid._exceptions import CancelledError, FailedPreconditionError
from buildgrid._protos.google.longrunning import operations_pb2
from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Action, Command, Digest


class MockScheduler:

    def __init__(self, messages=[]):
        self.messages = messages
        self._kwargs = None
        self._args = None

    def stream_operation_updates(self, *args, **kwargs):
        yield from self.messages

    def queue_job_action(self, *args, **kwargs):
        self._args = args
        self._kwargs = kwargs
        return "queued"

    def return_queue_call_args(self):
        return self._args, self._kwargs


class DeprecatedAction:

    def __init__(self, *, command_digest, input_root_digest, do_not_cache):
        self.command_digest = command_digest
        self.input_root_digest = input_root_digest
        self.do_not_cache = do_not_cache

    def HasField(self, field):
        return False


class MockStorage:

    def __init__(self, pairs):
        command_digest = Digest(hash="command_digest", size_bytes=0)
        input_root_digest = Digest(hash="input_root_digest", size_bytes=0)
        self.action = Action(
            command_digest=command_digest,
            input_root_digest=input_root_digest,
            do_not_cache=True
        )
        self.command = Command()
        for property_pair in pairs:
            prop = self.command.platform.properties.add()
            prop.name = property_pair[0]
            prop.value = property_pair[1]

            prop = self.action.platform.properties.add()
            prop.name = property_pair[0]
            prop.value = property_pair[1]

        self.action_no_platform = DeprecatedAction(
            command_digest=command_digest,
            input_root_digest=input_root_digest,
            do_not_cache=True
        )
        self.command_fetched = False

    def get_message(self, digest, *args, **kwargs):
        if digest == "fake":
            return self.action
        elif digest == "fake-no-platform":
            return self.action_no_platform
        else:
            self.command_fetched = True
            return self.command


def test_execute_platform_matching_simple():
    """Will match on standard keys."""

    pairs = [("OSFamily", "linux"), ("ISA", "x86-64"), ("ISA", "x86-avx")]
    keys = ["OSFamily", "ISA"]
    storage = MockStorage(pairs)
    exec_instance = ExecutionInstance(
        MockScheduler(), storage, property_keys=keys, match_properties=keys)
    assert exec_instance.execute("fake", False) == "queued"
    assert not storage.command_fetched


def test_execute_platform_matching_fallback():
    """Will match on standard keys."""

    pairs = [("OSFamily", "linux"), ("ISA", "x86-64"), ("ISA", "x86-avx")]
    keys = ["OSFamily", "ISA"]
    storage = MockStorage(pairs)
    exec_instance = ExecutionInstance(
        MockScheduler(), storage, property_keys=keys, match_properties=keys)
    assert exec_instance.execute("fake-no-platform", False) == "queued"
    assert storage.command_fetched


def test_execute_platform_matching_too_many_os():
    """Will not match due to too many OSFamilies being specified."""

    pairs = [("OSFamily", "linux"), ("OSFamily", "macos"),
             ("ISA", "x86-64"), ("ISA", "x86-avx")]
    keys = ["OSFamily", "ISA"]
    exec_instance = ExecutionInstance(
        MockScheduler(), MockStorage(pairs), property_keys=keys, match_properties=keys)
    with pytest.raises(FailedPreconditionError):
        exec_instance.execute("fake", False)


def test_execute_platform_matching_too_many_os_platform_key():
    """Make sure adding duplicate keys won't cause issues ."""

    pairs = [("OSFamily", "linux"), ("OSFamily", "macos"),
             ("ISA", "x86-64"), ("ISA", "x86-avx")]
    keys = ["OSFamily", "ISA", "OSFamily"]
    exec_instance = ExecutionInstance(
        MockScheduler(), MockStorage(pairs), property_keys=keys, match_properties=keys)
    with pytest.raises(FailedPreconditionError):
        exec_instance.execute("fake", False)


def test_execute_platform_matching_failure():
    """Will not match due to platform-keys missing 'ChrootDigest'."""

    pairs = [("OSFamily", "linux"), ("ISA", "x86-64"),
             ("ChrootDigest", "deadbeef")]
    keys = ["OSFamily", "ISA"]
    exec_instance = ExecutionInstance(
        MockScheduler(), MockStorage(pairs), property_keys=keys, match_properties=keys)
    with pytest.raises(FailedPreconditionError):
        exec_instance.execute("fake", False)


def test_execute_platform_matching_success():
    """Will match due to platform keys matching."""

    pairs = [("ChrootDigest", "deadbeed")]
    keys = ["OSFamily", "ISA", "ChrootDigest"]
    exec_instance = ExecutionInstance(
        MockScheduler(), MockStorage(pairs), property_keys=keys, match_properties=keys)
    assert exec_instance.execute("fake", False) == "queued"


def test_execute_platform_matching_config_only():
    """Will match due to platform keys matching."""

    pairs = [("OSFamily", "linux"), ("ISA", "x86-64"), ("ChrootDigest", "deadbeed")]
    keys = ["OSFamily", "ISA", "ChrootDigest"]
    scheduler = MockScheduler()
    exec_instance = ExecutionInstance(
        scheduler, MockStorage(pairs), property_keys=keys, match_properties=keys[0:2])
    assert exec_instance.execute("fake", False) == "queued"
    # The ChrootDigest key shouldn't make it into the actual requirements
    _, queue_kwargs = scheduler.return_queue_call_args()
    assert 'ChrootDigest' not in queue_kwargs['platform_requirements']
    assert all(key in queue_kwargs['platform_requirements'] for key in keys[0:2])


def test_execute_platform_matching_both_empty():
    """Edge case where nothing specified on either side."""

    pairs = []
    exec_instance = ExecutionInstance(
        MockScheduler(), MockStorage(pairs), property_keys=[], match_properties=[])
    assert exec_instance.execute("fake", False) == "queued"


def test_execute_platform_matching_no_job_req():
    """If job doesn't specify platform key requirements, it should always pass."""

    pairs = [("OSFamily", "linux"), ("ISA", "x86-64")]
    keys = ["OSFamily", "ISA", "ChrootDigest"]
    exec_instance = ExecutionInstance(
        MockScheduler(), MockStorage(pairs), property_keys=keys, match_properties=keys)
    assert exec_instance.execute("fake", False) == "queued"


def test_execute_priority_set():
    """Check that the priority gets set."""
    pairs = [("OSFamily", "linux"), ("ISA", "x86-64")]
    mock_scheduler = MockScheduler()
    keys = ["OSFamily", "ISA", "ChrootDigest"]
    exec_instance = ExecutionInstance(
        mock_scheduler, MockStorage(pairs), keys, keys)
    exec_instance.execute("fake", False, priority=3)
    _, kwargs = mock_scheduler.return_queue_call_args()
    assert 'priority' in kwargs
    assert kwargs['priority'] == 3


def test_execute_priority_default():
    """Check that the priority gets sets to 0 when not specified."""
    pairs = [("OSFamily", "linux"), ("ISA", "x86-64")]
    mock_scheduler = MockScheduler()
    keys = ["OSFamily", "ISA", "ChrootDigest"]
    exec_instance = ExecutionInstance(
        mock_scheduler, MockStorage(pairs), keys, keys)
    exec_instance.execute("fake", False)
    _, kwargs = mock_scheduler.return_queue_call_args()
    assert 'priority' in kwargs
    assert kwargs['priority'] == 0


@pytest.fixture
def mock_exec_instance():
    return ExecutionInstance(
        MockScheduler(), MockStorage([]), [], [])


@pytest.fixture
def mock_active_context():
    cxt = mock.MagicMock(spec=_Context)
    yield cxt


@pytest.fixture
def mock_operation():
    operation = mock.Mock(spec=operations_pb2.Operation)
    operation.done = False
    return operation


@pytest.fixture
def mock_operation_done():
    operation = mock.Mock(spec=operations_pb2.Operation)
    operation.done = True
    return operation


@pytest.fixture(params=[0, 1, 2])
def operation_updates_message_seq(mock_operation, request):
    seq = []
    for i in range(request.param):
        seq.append((None, mock_operation))

    return (request.param, seq)


@pytest.fixture(params=[0, 1, 2])
def operation_updates_completing_message_seq(mock_operation, mock_operation_done, request):
    seq = []
    for i in range(request.param):
        seq.append((None, mock_operation))

    seq.append((None, mock_operation_done))

    # Add sentinel operation update: this should never be dequeued
    # since we should stop once the operation completed (above)
    seq.append((ValueError, mock_operation))
    return (request.param, seq)


@pytest.fixture(params=[0, 1, 2])
def operation_updates_ending_with_error_seq(mock_operation, mock_operation_done, request):
    seq = []
    for i in range(request.param):
        seq.append((None, mock_operation))

    seq.append((CancelledError("Operation has been cancelled"), mock_operation))

    # Add sentinel operation update: this should never be dequeued
    # since we should stop once we encounter the first error
    seq.append((ValueError, mock_operation))
    return (request.param, seq)


def test_stream_operation_updates_stopsiteration_when_operation_done(operation_updates_completing_message_seq,
                                                                     mock_exec_instance, mock_active_context):
    """
        Make sure that `stream_operation_updates_while_context_is_active` raises
        a StopIteration once the operation has completed
    """
    n, msg_seq = operation_updates_completing_message_seq
    mock_exec_instance = ExecutionInstance(MockScheduler(msg_seq), MockStorage([]), [], [])

    generator = mock_exec_instance.stream_operation_updates(
        "test", mock_active_context)

    for i in range(n):
        seq_error, seq_operation = msg_seq[i]
        operation = next(generator)

        assert seq_operation == operation
        assert not operation.done

    # Operation completion
    seq_error, seq_operation = msg_seq[n]
    operation = next(generator)

    assert seq_operation == operation
    assert operation.done

    # After operation completion
    with pytest.raises(StopIteration):
        next(generator)


@pytest.fixture(params=[0, 1, 2, 3])
def n_0_to_3_inclusive(request):
    return request.param


def test_stream_operation_updates_stopsiteration_when_context_becomes_inactive(mock_operation,
                                                                               mock_active_context,
                                                                               n_0_to_3_inclusive):
    """
        Make sure that `stream_operation_updates_while_context_is_active` raises a StopIteration
        once the context closes even when there are more updates
    """
    n_yields_before_close = n_0_to_3_inclusive

    # Add more items than we'll yield before the context closes
    messages = [(None, mock_operation) for _ in range(n_yields_before_close * 2)]

    mock_exec_instance = ExecutionInstance(MockScheduler(messages), MockStorage([]), [], [])

    generator = mock_exec_instance.stream_operation_updates(
        "test", mock_active_context)

    # get first n
    for i in range(n_yields_before_close):
        operation = next(generator)
        assert operation == mock_operation

    # "Close" the context...
    mock_active_context.is_active = mock.MagicMock(return_value=False)

    # see that we get StopIteration right after closing the context
    with pytest.raises(StopIteration):
        next(generator)


def test_stream_operation_raises_and_stopsiteration_on_operation_errors(operation_updates_ending_with_error_seq,
                                                                        mock_active_context):
    """
        Make sure that `stream_operation_updates_while_context_is_active` raises
        operation update errors and then ends (raises StopIteration)
    """
    n, msg_seq = operation_updates_ending_with_error_seq

    mock_exec_instance = ExecutionInstance(MockScheduler(msg_seq), MockStorage([]), [], [])
    generator = mock_exec_instance.stream_operation_updates(
        "test", mock_active_context)

    for i in range(n):
        seq_error, seq_operation = msg_seq[i]
        operation = next(generator)

        assert seq_operation == operation
        assert not operation.done

    with pytest.raises(CancelledError):
        operation = next(generator)

    with pytest.raises(StopIteration):
        operation = next(generator)
