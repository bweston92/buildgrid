FROM debian:buster

RUN  [ \
    "apt-get", "update" ]
RUN  [ \
    "apt-get", "install", "-y", \
    "python3", "python3-venv", "python3-pip", \
    "bubblewrap", "fuse3", "postgresql", "postgresql-contrib" \
    ]

# Use /app as working directory:
WORKDIR /app

# Create a virtual environment:
RUN [ \
    "python3", "-m", "venv", "/app/env" \
    ]

# Upgrade Python core modules:
RUN [ \
    "/app/env/bin/python", "-m", "pip", \
    "install", "--upgrade", \
    "setuptools", "pip", "wheel" \
    ]

# Install the main requirements:
ADD requirements/requirements.txt /app
RUN [ \
    "/app/env/bin/python", "-m", "pip", \
    "install", "--requirement", \
    "requirements.txt" \
    ]

# Install the database requirements:
ADD requirements/requirements.db.txt /app
RUN [ \
    "/app/env/bin/python", "-m", "pip", \
    "install", "--requirement", \
    "requirements.db.txt" \
    ]

# Install the auth. requirements:
ADD requirements/requirements.auth.txt /app
RUN [ \
    "/app/env/bin/python", "-m", "pip", \
    "install", "--requirement", \
    "requirements.auth.txt" \
    ]

# Install the test requirements:
ADD requirements/requirements.tests.txt /app
RUN [ \
    "/app/env/bin/python", "-m", "pip", \
    "install", "--requirement", \
    "requirements.tests.txt" \
    ]

# Copy the repo. contents:
COPY . /app

# Install tests:
RUN [ \
    "/app/env/bin/python", "-m", "pip", \
    "install", "--editable", \
    ".[auth,tests]" \
    ]
