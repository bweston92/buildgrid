##
# BuildGrid's Docker Compose manifest.
#
#   ¡FOR LOCAL DEVELOPMENT ONLY!
#
# Spins-up a 'local' grid instance:
#   - Controller at http://localhost:50051 (via nginx to allow scaling)
#   - CAS at http://localhost:50052
#   - AC at http://localhost:50053
#
# Basic usage:
#  - docker-compose build
#  - docker-compose up --scale controller=2 --scale bots=10
#  - docker-compose down
#  - docker volume inspect buildgrid_data
#  - docker volume rm buildgrid_data
#  - docker image rm buildgrid:local
#
version: "3.2"

services:
  database:
    image: postgres:latest
    environment:
      POSTGRES_USER: bgd
      POSTGRES_PASSWORD: insecure
      POSTGRES_DB: bgd
    volumes:
      - type: volume
        source: db
        target: /var/lib/postgresql/data
    networks:
      - backend
    ports:
      - "5432:5432"

  storage:
    build:
      context: .
    image: buildgrid:local
    command: [
      "server", "start", "-vvv",
      "/app/config/storage.yml"]
    volumes:
      - type: volume
        source: data
        target: /var/lib/buildgrid/store
        volume:
          nocopy: true
      - type: bind
        source: ./data/config/storage.yml
        target: /app/config/storage.yml
    ports:
      - "50052:50052"
    networks:
      - backend
      - host

  cache:
    build:
      context: .
    image: buildgrid:local
    command: [
      "server", "start", "-vvv",
      "/app/config/cache.yml"]
    volumes:
      - type: bind
        source: ./data/config/cache.yml
        target: /app/config/cache.yml
    ports:
      - "50053:50053"
    networks:
      - backend
      - host

  controller:  # To be scaled horizontally
    build:
      context: .
    image: buildgrid:local
    command: [
      "server", "start", "-vvv",
      "/app/config/controller.yml"]
    volumes:
      - type: bind
        source: ./data/config/controller.yml
        target: /app/config/controller.yml
    expose:
      - "50051"
    depends_on:
      - database
    networks:
      - backend

  bots-interface:  # To be scaled horizontally
    build:
      context: .
    image: buildgrid:local
    command: [
      "server", "start", "-vvv",
      "/app/config/bots-interface.yml"]
    volumes:
      - type: bind
        source: ./data/config/bots-interface.yml
        target: /app/config/bots-interface.yml
    expose:
      - "50054"
    depends_on:
      - database
    networks:
      - backend

  logstream:
    build:
      context: .
    image: buildgrid:local
    command: [
      "server", "start", "-vvv",
      "/app/config/logstream.yml"]
    volumes:
      - type: bind
        source: ./data/config/logstream.yml
        target: /app/config/logstream.yml
    expose:
      - "50055"
    depends_on:
      - database
    networks:
      - backend
      - host

  bots:  # To be scaled horizontally
    image: buildgrid:local
    command: [
      "bot", "--instance-name=",
      "--remote=http://bots-interface:50054",
      "--remote-cas=http://storage:50052",
      "host-tools"]
    privileged: true
    volumes:
      - type: volume
        source: cache
        target: /var/lib/buildgrid/cache
      - type: bind
        source: ./data/bin
        target: /app/tools
    depends_on:
      - controller
    networks:
      - backend

  # We use nginx here to provide a proxy through to the controller,
  # rather than forcing clients to connect directly to the controller.
  # This allows us to use docker-compose --scale to horizontally scale
  # the controller, whilst keeping a single "controller" port exposed
  # for clients to use. This also allows us to use docker's DNS to do
  # some naive load balancing across controllers.
  nginx:
    image: nginx:latest
    volumes:
      - ./data/config/nginx.conf:/etc/nginx/nginx.conf:ro
    depends_on:
      - controller
    ports:
      - "50051:50051"
    networks:
      - backend
      - host

networks:
  backend:
  host:

volumes:
  cache:
  data:
  db:
