# Copyright (C) 2021 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import asyncio
from concurrent import futures
import logging
import logging.handlers
import os
import signal
import sys
from typing import Callable, Optional

import grpc
import janus

from buildgrid._exceptions import PermissionDeniedError
from buildgrid.server.rabbitmq.bots.service import BotsService
from buildgrid.settings import (
    LOG_RECORD_FORMAT,
    MIN_THREAD_POOL_SIZE
)


class RMQServer:
    """Creates a BuildGrid server instance that is powered by RabbitMQ.

    The :class:`RMQServer` class binds together all the gRPC services.
    """

    def __init__(self, *, max_workers: Optional[int]=None, enable_metrics: bool=False, **kwargs):
        """Initializes a new :class:`RMQServer` instance.
        """
        self._logger = logging.getLogger(__name__)

        self._main_loop = asyncio.get_event_loop()

        self._logging_queue: janus.Queue = janus.Queue(loop=self._main_loop)
        self._logging_handler = logging.handlers.QueueHandler(self._logging_queue.sync_q)  # type: ignore
        self._logging_formatter = logging.Formatter(fmt=LOG_RECORD_FORMAT)
        self._logging_task: Optional[asyncio.Future] = None

        self._bots_service = None

        self._enable_metrics = enable_metrics

        # Setup the main logging handler:
        self._setup_logging()

        if max_workers is None:
            # Use max_workers default from Python 3.4+
            max_workers = max(MIN_THREAD_POOL_SIZE, (os.cpu_count() or 1) * 5)

        elif max_workers < MIN_THREAD_POOL_SIZE:
            self._logger.warning(f"Specified thread-limit=[{max_workers}] is too small, "
                                 f"bumping it to [{MIN_THREAD_POOL_SIZE}]")
            # Enforce a minumun for max_workers
            max_workers = MIN_THREAD_POOL_SIZE

        try:
            # pylint: disable=consider-using-with
            self._grpc_executor = futures.ThreadPoolExecutor(
                max_workers, thread_name_prefix="gRPC_Executor")
        except TypeError:
            # We need python >= 3.6 to support `thread_name_prefix`, so fallback
            # to ugly thread names if that didn't work

            # pylint: disable=consider-using-with
            self._grpc_executor = futures.ThreadPoolExecutor(max_workers)

        # NOTE: maximum_concurrent_rpcs is set equal to max_workers to avoid
        # deadlocking remote execution when multiple services being run in
        # one process. eg. A server running CAS, Execution, and Bots services
        # can get into a state where all the executor threads are handling
        # long-lived execution connections. Allowing more concurrent RPCs
        # will queue CAS or Bots service requests until they timeout in this
        # scenario, rather than failing fast.
        self._grpc_server = grpc.server(
            self._grpc_executor, maximum_concurrent_rpcs=max_workers)

    def _setup_logging(self) -> None:
        root_logger = logging.getLogger()

        for log_filter in root_logger.filters[:]:
            self._logging_handler.addFilter(log_filter)
            root_logger.removeFilter(log_filter)

        for log_handler in root_logger.handlers[:]:
            root_logger.removeHandler(log_handler)
        root_logger.addHandler(self._logging_handler)

    async def _logging_worker(self) -> None:
        """Worker to read messages off the logging queue and write them to stdout.

        """
        async def _logging_worker() -> None:
            log_record = await self._logging_queue.async_q.get()
            record = self._logging_formatter.format(log_record)

            # TODO: Investigate if async write would be worth here.
            sys.stdout.write(f'{record}\n')
            sys.stdout.flush()

            # TODO: publish log records to a monitoring bus

        while True:
            try:
                await _logging_worker()

            except asyncio.CancelledError:
                break
            except Exception:
                # The thread shouldn't exit on exceptions, but log at a severe enough level
                # that it doesn't get lost in logs
                self._logger.exception("Exception in logging worker")

    # --- Public API ---
    def start(self, *, on_server_start_cb: Callable=None) -> None:
        """Starts the BuildGrid server."""
        # Call the start method of the gRPC services, so they can start up any
        # background threads they need
        if self._bots_service is not None:
            self._bots_service.start()

        # Start the gRPC server and call the on-start callback
        self._grpc_server.start()
        if on_server_start_cb:
            on_server_start_cb()

        # TODO: Start the monitoring bus

        self._logging_task = asyncio.ensure_future(
            self._logging_worker(), loop=self._main_loop)

        self._main_loop.add_signal_handler(signal.SIGTERM, self.stop)

        self._main_loop.run_forever()

    def stop(self):
        """Stops the BuildGrid server."""
        # TODO: Stop the monitoring bus tasks
        if self._logging_task is not None:
            self._logging_task.cancel()

        # Stop the gRPC server to prevent new incoming requests
        self._grpc_server.stop(None)

        # Stop the gRPC services, to cleanly shut down background threads
        self._bots_service.stop()

        self._main_loop.stop()

    def add_port(self, address, credentials):
        """Adds a port to the server.

        Must be called before the server starts. If a credentials object exists,
        it will make a secure port.

        Args:
            address (str): The address with port number.
            credentials (:obj:`grpc.ChannelCredentials`): Credentials object.

        Returns:
            int: Number of the bound port.

        Raises:
            PermissionDeniedError: If socket binding fails.
        """
        if credentials is not None:
            self._logger.info(f"Adding secure connection on: [{address}]")
            port_number = self._grpc_server.add_secure_port(address, credentials)

        else:
            self._logger.info(f"Adding insecure connection on [{address}]")
            port_number = self._grpc_server.add_insecure_port(address)

        if not port_number:
            raise PermissionDeniedError("Unable to configure socket")

        return port_number

    def add_execution_instance(self, instance, instance_name):
        """Adds an :obj:`ExecutionInstance` to the service.

        If no service exists, it creates one.

        Args:
            instance (:obj:`ExecutionInstance`): Instance to add.
            instance_name (str): Instance name.
        """
        pass

    def add_bots_instance(self, instance, instance_name):
        """Adds a :obj:`BotsInstance` to the service.

        If no service exists, it creates one.

        Args:
            instance (:obj:`BotsInstance`): Instance to add.
            instance_name (str): Instance name.
        """
        if self._bots_service is None:
            self._bots_service = BotsService(self._grpc_server, self._enable_metrics)
        self._bots_service.add_instance(instance_name, instance)

    def add_operations_instance(self, instance, instance_name):
        """Adds an :obj:`OperationsInstance` to the service.

        If no service exists, it creates one.

        Args:
            instance (:obj:`OperationsInstance`): Instance to add.
            instance_name (str): Instance name.
        """
        pass

    def add_action_cache_instance(self, instance, instance_name):
        """Adds a :obj:`ReferenceCache` to the service.

        If no service exists, it creates one.

        Args:
            instance (:obj:`ReferenceCache`): Instance to add.
            instance_name (str): Instance name.
        """
        pass

    def add_cas_instance(self, instance, instance_name):
        """Adds a :obj:`ContentAddressableStorageInstance` to the service.

        If no service exists, it creates one.

        Args:
            instance (:obj:`ReferenceCache`): Instance to add.
            instance_name (str): Instance name.
        """
        pass

    def add_bytestream_instance(self, instance, instance_name):
        """Adds a :obj:`ByteStreamInstance` to the service.

        If no service exists, it creates one.

        Args:
            instance (:obj:`ByteStreamInstance`): Instance to add.
            instance_name (str): Instance name.
        """
        pass

    def add_logstream_instance(self, instance, instance_name):
        """Adds a :obj:`LogStreamInstance` to the service.

        If no service exists, it creates one.

        Args:
            instance (:obj:`LogStreamInstance`): Instance to add.
            instance_name (str): The name of the instance being added.

        """
        pass

    # --- Public API: Monitoring ---
    @property
    def is_instrumented(self):
        pass
