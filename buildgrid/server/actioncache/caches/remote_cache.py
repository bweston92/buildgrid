# Copyright (C) 2019, 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Remote Action Cache
===================

Provides an interface to a remote Action Cache. This can be used by other
services (e.g. an Execution service) to communicate with a remote cache.

It provides the same API as any other Action Cache instance backend.

"""


import logging

from buildgrid._exceptions import NotFoundError
from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import (
    ActionResult,
    Digest
)
from buildgrid.client.actioncache import ActionCacheClient
from buildgrid.client.capabilities import CapabilitiesInterface
from buildgrid.server.actioncache.caches.action_cache_abc import ActionCacheABC


class RemoteActionCache(ActionCacheABC):

    def __init__(self, channel, instance_name: str):
        """Initialises a new RemoteActionCache instance.

        Args:
            channel (grpc.channel): Channel to use for communication with the
                remote ActionCache service.
            instance_name (str): The instance name of the remote ActionCache
                service.

        """
        ActionCacheABC.__init__(self)
        self._logger = logging.getLogger(__name__)
        self._remote_instance_name = instance_name
        self._channel = channel
        self._allow_updates = None

        self._action_cache = ActionCacheClient(
            self._channel, self._remote_instance_name)

    @property
    def allow_updates(self) -> bool:
        # Check if updates are allowed if we haven't already.
        # This is done the first time update_action_result is called rather
        # than on instantiation because the remote cache may not be running
        # when this object is instantiated.
        if self._allow_updates is None:
            interface = CapabilitiesInterface(self._channel)
            capabilities = interface.get_capabilities(self._remote_instance_name)
            self._allow_updates = (capabilities
                                   .cache_capabilities
                                   .action_cache_update_capabilities
                                   .update_enabled)
        return self._allow_updates

    def get_action_result(self, action_digest: Digest) -> ActionResult:
        """Retrieves the cached result for an Action.

        Queries the remote ActionCache service to retrieve the cached
        result for a given Action digest. If the remote cache doesn't
        contain a result for the Action, then ``None`` is returned.

        Args:
            action_digest (Digest): The digest of the Action to retrieve the
                cached result of.

        """
        action_result = self._action_cache.get(action_digest)

        if action_result is None:
            key = self._get_key(action_digest)
            raise NotFoundError(f"Key not found: {key}")
        return action_result

    def update_action_result(self, action_digest: Digest,
                             action_result: ActionResult) -> None:
        """Stores a result for an Action in the remote cache.

        Sends an ``UpdateActionResult`` request to the remote ActionCache
        service, to store the result in the remote cache.

        If the remote cache doesn't allow updates, then this raises a
        ``NotImplementedError``.

        Args:
            action_digest (Digest): The digest of the Action whose result is
                being cached.
            action_result (ActionResult): The result to cache for the given
                Action digest.

        """
        if not self.allow_updates:
            raise NotImplementedError("Updating cache not allowed")
        return self._action_cache.update(action_digest, action_result)

    def _get_key(self, action_digest):
        """Get a hashable cache key from a given Action digest.

        Args:
            action_digest (Digest): The digest to produce a cache key for.

        """
        return (action_digest.hash, action_digest.size_bytes)
