# Copyright (C) 2021 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from aiohttp_middlewares.cors import ACCESS_CONTROL_ALLOW_CREDENTIALS, ACCESS_CONTROL_ALLOW_ORIGIN


TARBALL_DIRECTORY_PREFIX = 'bgd-browser-tarball-'


def get_cors_headers(origin, origins, allow_all):
    headers = {}
    if origin:
        headers[ACCESS_CONTROL_ALLOW_CREDENTIALS] = "true"
        if origin in origins:
            headers[ACCESS_CONTROL_ALLOW_ORIGIN] = origin
        elif allow_all:
            headers[ACCESS_CONTROL_ALLOW_ORIGIN] = "*"
    return headers
